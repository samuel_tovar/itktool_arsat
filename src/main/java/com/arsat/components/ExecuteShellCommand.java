package com.arsat.components;

import org.slf4j.Logger;

public interface ExecuteShellCommand {
    public String executeCommand(String command, Logger log);
}
