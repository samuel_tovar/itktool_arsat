package com.arsat.components;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class ExecuteShellCommandImpl implements ExecuteShellCommand {

    public String executeCommand(String command, Logger log) {
        System.out.println(command);
        StringBuffer output = new StringBuffer();

        Process p;
        try {
            log.info("\nEjecutando "+command);
            System.out.println("Ejecutando");
            p = Runtime.getRuntime().exec(command);

            Thread.sleep(10000);
            p.destroy();
            p.waitFor();
            System.out.println("\nSalida "+p.getInputStream());
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(p.getInputStream()));
            System.out.println("\nBuffer "+reader.toString());
            String line = "";
            while ((line = reader.readLine())!= null) {
                output.append(line + "\n");
            }

        } catch (IOException ex){
            System.out.println("IOExeption"+ex.toString());
            log.info("\nIOExeption "+ex.toString());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Exeption"+e.toString());
            log.info("\nExeption "+e.toString());
        }
        System.out.println("Salida"+output.toString());
        log.info("\nSalida "+output.toString());
        return output.toString();

    }

}