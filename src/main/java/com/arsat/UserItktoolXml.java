package com.arsat;

import com.arsat.entities.UserItktool;
import org.slf4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.time.LocalDate;
import java.util.List;

public class UserItktoolXml {
    //public static final String xmlFilePath = "/itktool/xmlusers.xml";
    public boolean crearXml(List<UserItktool> listUser , Logger log , String xmlFilePath) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

            Document document = documentBuilder.newDocument();
            // root element
            log.info("\nDocument Root Users_list");
            Element root = document.createElement("users_list");
            document.appendChild(root);

            Element version = document.createElement("version");
            root.appendChild(version);

            Element create = document.createElement("create");
            root.appendChild(create);
            Element datetime = document.createElement("datetime");
            create.appendChild(datetime);
            LocalDate today = LocalDate.now();
            String yeard = String.valueOf(today.getYear());
            String monthd = String.valueOf(today.getMonthValue());
            String dayd = String.valueOf(today.getDayOfMonth());
            String weekd = "6";//String.valueOf(today.getDayOfWeek());
            Element year = document.createElement("year");
            year.appendChild(document.createTextNode(yeard));
            datetime.appendChild(year);

            Element month = document.createElement("month");
            month.appendChild(document.createTextNode(monthd));
            datetime.appendChild(month);

            Element day = document.createElement("day");
            day.appendChild(document.createTextNode(dayd));
            datetime.appendChild(day);

            Element dweek = document.createElement("dweek");
            dweek.appendChild(document.createTextNode(weekd));
            datetime.appendChild(dweek);

            Element hour = document.createElement("hour");
            hour.appendChild(document.createTextNode("19"));
            datetime.appendChild(hour);

            Element minute = document.createElement("minute");
            minute.appendChild(document.createTextNode("06"));
            datetime.appendChild(minute);

            Element seconds = document.createElement("seconds");
            seconds.appendChild(document.createTextNode("14"));
            datetime.appendChild(seconds);

            Element xmluser = document.createElement("xmluser");
            xmluser.appendChild(document.createTextNode("0.9.4"));
            version.appendChild(xmluser);

            Element libitk = document.createElement("libitkcom");
            libitk.appendChild(document.createTextNode("1.1.15"));
            version.appendChild(libitk);

            Element itktool = document.createElement("itktool");
            itktool.appendChild(document.createTextNode("1.0.19"));
            version.appendChild(itktool);

            Element userEle;
            log.info("\nDocument Element user"+listUser.size());
            for (UserItktool user : listUser) {
                userEle = document.createElement("user");

                Element acces_id = document.createElement("access_id");
                acces_id.appendChild(document.createTextNode(user.getAccess_id().toString()));
                userEle.appendChild(acces_id);

                Element password = document.createElement("password");
                password.appendChild(document.createTextNode(user.getPassword().toString()));
                userEle.appendChild(password);
                Element status = document.createElement("status");
                status.appendChild(document.createTextNode(String.valueOf(user.getStatus())));
                userEle.appendChild(status);
                Element acces_ctl = document.createElement("access_ctl");
                acces_ctl.appendChild(document.createTextNode(String.valueOf(user.getAccess_ctl())));
                userEle.appendChild(acces_ctl);
                Element panic = document.createElement("panic");
                panic.appendChild(document.createTextNode(String.valueOf(user.getPanic())));
                userEle.appendChild(panic);

                Element bio_count = document.createElement("bio_count");
                bio_count.appendChild(document.createTextNode(String.valueOf(user.getBio_count())));
                userEle.appendChild(bio_count);
                Element bio_level = document.createElement("bio_level");
                bio_level.appendChild(document.createTextNode(String.valueOf(user.getBio_level())));
                userEle.appendChild(bio_level);
                Element sec_level = document.createElement("sec_level");
                sec_level.appendChild(document.createTextNode(String.valueOf(user.getSec_level())));
                userEle.appendChild(sec_level);
                Element user_name = document.createElement("user_name");
                user_name.appendChild(document.createTextNode(user.getUser_name()));
                userEle.appendChild(user_name);

                Element schedule_id = document.createElement("schedule_id");
                schedule_id.appendChild(document.createTextNode(String.valueOf(user.getSchedule_id())));
                userEle.appendChild(schedule_id);
                Element anti_passback = document.createElement("anti_passback");
                anti_passback.appendChild(document.createTextNode(String.valueOf(user.getAnti_passback())));
                userEle.appendChild(anti_passback);

                Element template_data1 = document.createElement("template");
                Attr attr = document.createAttribute("id");
                attr.setValue("0");
                template_data1.setAttributeNode(attr);
                Element data1 = document.createElement("data");
                String huella1 = String.valueOf(user.getTemplate_data1()).isEmpty()?"0":String.valueOf(user.getTemplate_data1());
                data1.appendChild(document.createTextNode(huella1));
                template_data1.appendChild(data1);

                if(!huella1.equalsIgnoreCase("null") && !huella1.equalsIgnoreCase("0"))
                    userEle.appendChild(template_data1);

                Element template_data2 = document.createElement("template");
                Attr attr2 = document.createAttribute("id");
                attr2.setValue("1");
                template_data2.setAttributeNode(attr2);
                Element data2 = document.createElement("data");

                String huella2 = String.valueOf(user.getTemplate_data2()).isEmpty()?"0":String.valueOf(user.getTemplate_data2());
                data2.appendChild(document.createTextNode(huella2));
                template_data2.appendChild(data2);
                if(!huella2.equalsIgnoreCase("null") && !huella2.equalsIgnoreCase("0"))
                    userEle.appendChild(template_data2);

                root.appendChild(userEle);
            }

            // user element
            log.info("\ncreate the xml file "+xmlFilePath);
            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(new File(xmlFilePath));

            transformer.transform(domSource, streamResult);
            log.info("\nDone creating XML File");
            System.out.println("Done creating XML File");
            return true;
        }catch (ParserConfigurationException ex){
            log.info("\nError creating XML File"+ex.getMessage());
            return false;
        }catch (TransformerException ext){
            log.info("\ntransformerException creating XML File"+ext.getMessage());
            return false;
        }
    }

    }
