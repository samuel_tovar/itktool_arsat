package com.arsat.repositories;

import com.arsat.entities.Gerencia;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GerenciaRepository  extends JpaRepository<Gerencia, Long> {
}
