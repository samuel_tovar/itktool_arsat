package com.arsat.repositories;

import com.arsat.entities.Empleado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado,Long> {
    @Query("SELECT u FROM Empleado u WHERE state IN (:nuevo)")
    List<Empleado> findByEstado(@Param("nuevo") int nuevo);

    @Query("SELECT u FROM Empleado u WHERE badgeAccess IN (:access_id)")
    Empleado existByAccess(@Param("access_id") Long accessId);
}
