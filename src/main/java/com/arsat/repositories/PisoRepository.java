package com.arsat.repositories;

import com.arsat.entities.Piso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PisoRepository extends JpaRepository<Piso, Long> {

    @Query("SELECT p FROM Piso p WHERE piso = :piso")
    Piso findByPiso(@Param("piso") String piso);
    @Query("SELECT p FROM Piso p WHERE area = :area")
    List<Piso> findByArea(@Param("area") String area);
    @Query("SELECT p FROM Piso p WHERE state = :state")
    List<Piso> findAllState(@Param("state") Integer state);
}
