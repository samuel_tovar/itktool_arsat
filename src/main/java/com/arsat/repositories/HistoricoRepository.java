package com.arsat.repositories;

import com.arsat.entities.Historico;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface HistoricoRepository extends CrudRepository<Historico, Long> {
    @Query("SELECT u FROM Historico u WHERE fecha= :fecha and entrada_salida= :entrada_salida and access_id= :access_id ")
    Historico findByhistorico(@Param("access_id") int access_id, @Param("entrada_salida") int entrada_salida, @Param("fecha") Date fecha);
}
