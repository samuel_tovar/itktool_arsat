package com.arsat.repositories;

import com.arsat.entities.UserItktool;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserItktoolRepository extends CrudRepository<UserItktool, Long> {


   @Query("SELECT u FROM UserItktool u WHERE estado IN (:nuevo)")
   List<UserItktool> findByEstado(@Param("nuevo") int nuevo);

   @Query("SELECT u FROM UserItktool u WHERE access_id IN (:access_id)")
   UserItktool existByAccess(@Param("access_id") Long accessId);

}
