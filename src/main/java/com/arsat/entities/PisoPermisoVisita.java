package com.arsat.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="piso_permiso")
public class PisoPermisoVisita implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int id;
    private Integer state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference(value = "visitaJson")
    @JoinColumn(name = "badge_access",referencedColumnName = "badge_access")
    private Visita visita;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonBackReference(value = "pisovJson")
    @JoinColumn(name = "piso_id",referencedColumnName = "id")
    private PisoVisita piso;

    private Long pisoId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getPisoId() {
        return pisoId;
    }

    public void setPisoId(Long pisoId) {
        this.pisoId = pisoId;
    }

    public PisoPermisoVisita(PisoVisita piso,Integer state){
        this.piso=piso;
        this.setState(state);
    }

    public PisoVisita getPiso() {
        return piso;
    }

    public void setPiso(PisoVisita piso) {
        this.piso = piso;
    }

    public Visita getVisita() {
        return visita;
    }

    public void setVisita(Visita visita) {
        this.visita = visita;
    }
}
