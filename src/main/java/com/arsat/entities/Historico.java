package com.arsat.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="historicos")
public class Historico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int id;
    public int access_id;
    public Date fecha;
    public int entrada_salida;
    @OneToOne
    @JoinColumn(name = "piso_id", referencedColumnName = "id")
    private Piso piso;
    public int status;

    public Historico(int access_id, Date fecha, int entrada_salida, Piso piso, int status) {
        this.access_id = access_id;
        this.fecha = fecha;
        this.entrada_salida = entrada_salida;
        this.piso = piso;
        this.status = status;
    }

    public Historico(int id, int access_id, Date fecha, int entrada_salida, Piso piso, int status) {
        this.id = id;
        this.access_id = access_id;
        this.fecha = fecha;
        this.entrada_salida = entrada_salida;
        this.piso = piso;
        this.status = status;
    }

    public Historico() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAccess_id() {
        return access_id;
    }

    public void setAccess_id(int access_id) {
        this.access_id = access_id;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getEntrada_salida() {
        return entrada_salida;
    }

    public void setEntrada_salida(int entrada_salida) {
        this.entrada_salida = entrada_salida;
    }

    public Piso getPiso() {
        return piso;
    }

    public void setPiso(Piso piso) {
        this.piso = piso;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
