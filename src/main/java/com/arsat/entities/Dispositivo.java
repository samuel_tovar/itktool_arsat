package com.arsat.entities;


import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="dispositivos")
public class Dispositivo  implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int id;
    private Integer state;
    @Column(name = "ip", nullable = false)
    public String ip;
    @Column(name = "port", nullable = false)
    public int port;

    private String descripcion;

    private Boolean ping;


    @OneToOne
    @JoinColumn(name = "piso_id", referencedColumnName = "id")
    private Piso piso;
    @Column(name = "entrada_salida", nullable = false)
    public int entradaSalida;


    public Dispositivo(int id, String ip, int port, int status, String piso,int entradaSalida) {
        id = id;
        ip = ip;
        port = port;
        state = status;
        piso = piso;
        entradaSalida = entradaSalida;
    }

    public Dispositivo() {
    }

    public Dispositivo(String ip, int status, String piso) {
        ip = ip;
        state = status;
        piso = piso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        ip = ip;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        port = port;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getPing() {
        return ping;
    }

    public void setPing(Boolean ping) {
        this.ping = ping;
    }

    public Piso getPiso() {
        return piso;
    }

    public void setPiso(Piso piso) {
        this.piso = piso;
    }

    public int getEntradaSalida() {
        return entradaSalida;
    }

    public void setEntradaSalida(int entradaSalida) {
        this.entradaSalida = entradaSalida;
    }


}
