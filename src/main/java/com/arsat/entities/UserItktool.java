package com.arsat.entities;

import javax.persistence.*;

@Entity
@Table(name = "useritktool")
public class UserItktool {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Long access_id;
    private Long password;
    private int status;

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    private int estado;
    private int access_ctl;
    private int panic;
    private int bio_count;
    private int bio_level;
    private int sec_level;
    private String user_name;
    private int schedule_id;
    private int anti_passback;
    private String template_data1;
    private String template_data2;

    //@ManyToOne(fetch = FetchType.LAZY)
    //@JoinColumn(name = "access_id", insertable = false, updatable = false)
    //@Fetch(FetchMode.JOIN)
    //private Piso piso;

    public UserItktool() {
    }

    public UserItktool(Long id,Long access_id, Long password, int status, int access_ctl, int panic, int bio_count, int bio_level, int sec_level, String user_name, int schedule_id, int anti_passback, String template_data1, String template_data2) {
        this.id = id;
        this.access_id = access_id;
        this.password = password;
        this.status = status;
        this.access_ctl = access_ctl;
        this.panic = panic;
        this.bio_count = bio_count;
        this.bio_level = bio_level;
        this.sec_level = sec_level;
        this.user_name = user_name;
        this.schedule_id = schedule_id;
        this.anti_passback = anti_passback;
        this.template_data1 = template_data1;
        this.template_data2 = template_data2;
    }

    public UserItktool(Long access_id, Long password, int status, int access_ctl, int panic, int bio_count, int bio_level, int sec_level, String user_name, int schedule_id, int anti_passback, String template_data1, String template_data2) {
        this.access_id = access_id;
        this.password = password;
        this.status = status;
        this.access_ctl = access_ctl;
        this.panic = panic;
        this.bio_count = bio_count;
        this.bio_level = bio_level;
        this.sec_level = sec_level;
        this.user_name = user_name;
        this.schedule_id = schedule_id;
        this.anti_passback = anti_passback;
        this.template_data1 = template_data1;
        this.template_data2 = template_data2;
    }

    public Long getAccess_id() {
        return access_id;
    }

    public void setAccess_id(Long access_id) {
        this.access_id = access_id;
    }

    public Long getPassword() {
        return password;
    }

    public void setPassword(Long password) {
        this.password = password;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getAccess_ctl() {
        return access_ctl;
    }

    public void setAccess_ctl(int access_ctl) {
        this.access_ctl = access_ctl;
    }

    public int getPanic() {
        return panic;
    }

    public void setPanic(int panic) {
        this.panic = panic;
    }

    public int getBio_count() {
        return bio_count;
    }

    public void setBio_count(int bio_count) {
        this.bio_count = bio_count;
    }

    public int getBio_level() {
        return bio_level;
    }

    public void setBio_level(int bio_level) {
        this.bio_level = bio_level;
    }

    public int getSec_level() {
        return sec_level;
    }

    public void setSec_level(int sec_level) {
        this.sec_level = sec_level;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public int getSchedule_id() {
        return schedule_id;
    }

    public void setSchedule_id(int schedule_id) {
        this.schedule_id = schedule_id;
    }

    public int getAnti_passback() {
        return anti_passback;
    }

    public void setAnti_passback(int anti_passback) {
        this.anti_passback = anti_passback;
    }

    public String getTemplate_data1() {
        return template_data1;
    }

    public void setTemplate_data1(String template_data1) {
        this.template_data1 = template_data1;
    }

    public String getTemplate_data2() {
        return template_data2;
    }

    public void setTemplate_data2(String template_data2) {
        this.template_data2 = template_data2;
    }
}