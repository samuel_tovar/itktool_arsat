package com.arsat.services;

import com.arsat.entities.Historico;
import com.arsat.entities.Piso;
import com.arsat.entities.UserItktool;

import java.io.File;
import java.util.List;

public interface FileService {
    public void cargarXml(File file, List<UserItktool> listUser);
    public void cargarHistory(File file, List<Historico> listHistory, int entrada_salida, Piso piso);
    public String completar(String n);
}
