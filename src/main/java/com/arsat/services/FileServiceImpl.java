package com.arsat.services;

import com.arsat.entities.Historico;
import com.arsat.entities.Piso;
import com.arsat.entities.UserItktool;
import org.jdom2.Content;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class FileServiceImpl implements FileService {

    @Override
    public void cargarXml(File file, List<UserItktool> listUser) {
            SAXBuilder builder = new SAXBuilder();
            File xmlFile = file;
            try
            {

                Document document = (Document) builder.build( xmlFile );

                Element rootNode = document.getRootElement();
                List<Content> list = rootNode.getContent();
                Element campo;
                String template_data1="";
                String template_data2="";
                int count=1;
                int anti_passback=0;
                //Se recorre la lista de hijos de 'tables'
                for ( int i = 0; i < list.size(); i++ )
                {
                    if(list.get(i).getCType()== Content.CType.Element){
                        //Se obtiene el elemento 'tabla'
                        Element tabla = (Element) list.get(i);
                        String nombreTabla = tabla.getAttributeValue("user");

                        System.out.println( "Tabla: " + nombreTabla );
                        if(tabla.getName().equals("user")){
                            List lista_campos = tabla.getChildren();

                            if(lista_campos.size()>0){
                                count++;
                                campo = (Element)lista_campos.get(0);
                                Long access_id = new Long(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(1);
                                Long password = new Long(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(2);
                                int status = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(3);
                                int access_ctl = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(4);
                                int panic = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(5);
                                int bio_count = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(6);
                                int bio_level = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(7);
                                int sec_level = Integer.parseInt(campo.getContent().get(0).getValue());
                                campo = (Element)lista_campos.get(8);
                                String user_name = campo.getContent().get(0).getValue();
                                campo = (Element)lista_campos.get(9);
                                int schedule_id = Integer.parseInt(campo.getContent().get(0).getValue());
                                if(lista_campos.size()>11){
                                    template_data1=((Element) lista_campos.get(11)).getChildTextTrim("data");
                                    if(lista_campos.size()>12)
                                        template_data2=((Element) lista_campos.get(12)).getChildTextTrim("data");
                                    else
                                        template_data2="";
                                }else{
                                    template_data1="";
                                    template_data2="";
                                }

                                listUser.add(new UserItktool(access_id,password,status,access_ctl,panic,bio_count,bio_level,sec_level,user_name,schedule_id,anti_passback,template_data1,template_data2));

                            }
                        }

                    }

                }
            }catch ( IOException io ) {
                System.out.println( io.getMessage() );
            }catch ( JDOMException jdomex ) {
                System.out.println( jdomex.getMessage() );
            }
    }

    @Override
    public void cargarHistory(File file, List<Historico> listHistory, int entrada_salida, Piso piso) {
        SAXBuilder builder = new SAXBuilder();
        File xmlFile = file;
        try {

            Document document = (Document) builder.build(xmlFile);

            Element rootNode = document.getRootElement();
            List<Content> list = rootNode.getContent();
            Element campo;
            int count = 1;
            //Se recorre la lista de hijos de 'tables'
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getCType() == Content.CType.Element) {
                    //Se obtiene el elemento 'tabla'
                    Element tabla = (Element) list.get(i);
                    String nombreTabla = tabla.getAttributeValue("mark");

                    System.out.println("Tabla: " + nombreTabla);
                    if (tabla.getName().equals("mark")) {
                        List lista_campos = tabla.getChildren();

                        if(lista_campos.size()>0) {
                            count++;
                            campo = (Element) lista_campos.get(0);
                            int type = Integer.parseInt(campo.getContent().get(0).getValue());
                            campo = (Element) lista_campos.get(1);
                            Long access_id = new Long(campo.getContent().get(0).getValue());
                            campo = (Element) lista_campos.get(2);
                            int event_code = Integer.parseInt(campo.getContent().get(0).getValue());
                            campo = (Element) lista_campos.get(3);
                            int direction = Integer.parseInt(campo.getContent().get(0).getValue());
                            campo = (Element) lista_campos.get(4);
                            int source = Integer.parseInt(campo.getContent().get(0).getValue());
                           // campo = (Element) lista_campos.get(5);
                            String year =((Element) lista_campos.get(5)).getChildTextTrim("year");
                            String mes =((Element) lista_campos.get(5)).getChildTextTrim("month");
                            String dia =completar(((Element) lista_campos.get(5)).getChildTextTrim("day"));
                            String hora =completar(((Element) lista_campos.get(5)).getChildTextTrim("hour"));
                            String minuto =completar(((Element) lista_campos.get(5)).getChildTextTrim("minute"));
                            String segundo =completar(((Element) lista_campos.get(5)).getChildTextTrim("seconds"));
                            String date_s= dia+"-"+mes+"-"+year+" "+hora+":"+minuto+":"+segundo;
                            SimpleDateFormat dt = new SimpleDateFormat("dd-M-yy HH:mm:ss");
                            Date date = dt.parse(date_s);
                            listHistory.add(new Historico(access_id.intValue(),date,entrada_salida,piso,1));

                        }
                    }
                }
            }
            }catch(IOException io ){
                System.out.println(io.getMessage());
            }catch(JDOMException jdomex ){
                System.out.println(jdomex.getMessage());
            }catch (ParseException pe){
                System.out.println(pe.getMessage());
            }
        }
        public String completar(String n){
            if (n.length() <= 1)
            {
                return ("0"+n);
            }
            else
                return n;
        }
}
