package com.arsat.services;

import com.arsat.entities.UserItktool;
import org.slf4j.Logger;

import java.util.List;

public interface ItktoolService {

    public void sendNewData(Logger log);

    public void sendEditData(Logger log);

    public void sendDeleteData(Logger log);

    public void sendHistoryData(FileService fileServ,Logger log);

    public void saveFull(List<UserItktool> listUser);

    public void savePiso3(List<UserItktool> listUser);
}
