package com.arsat.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("asyncService")
@EnableAsync
public class AsyncServiceImpl implements AsyncService {
    private static final Logger logger = LoggerFactory.getLogger(AsyncServiceImpl.class);

    @Autowired
    private ItktoolServiceImpl itktoolService;
    @Autowired
    private FileService fileServ;

    @Override
    @Async("asynchThreadPoolTaskExecutor")
    public void processNew() throws InterruptedException {
        logger.info("async new Data started: " + new Date() + " threadId:" + Thread.currentThread().getId());
        System.out.println("\nfindByEstado(n)  1 -- nuevo ");
        Thread.sleep(1000);
        itktoolService.sendNewData(logger);
        logger.info("async new Data finished: " + new Date() + " threadId:" + Thread.currentThread().getId());
        return ;
    }

    @Override
    @Async("asynchThreadPoolTaskExecutor")
    public void processEdit() throws InterruptedException {
        logger.info("async edit delete Data started: " + new Date() + " threadId:" + Thread.currentThread().getId());
        System.out.println("\nfindByEstado(n)  2 -- modificado ");
        itktoolService.sendEditData(logger);
        Thread.sleep(5000);
        System.out.println("\nfindByEstado(n)  3 -- borrado ");
        itktoolService.sendDeleteData(logger);
        logger.info("async edit delete Data finished: " + new Date() + " threadId:" + Thread.currentThread().getId());
        return ;
    }

    @Override
    @Async("asynchThreadPoolTaskExecutor")
    public void processHistory() throws InterruptedException {
        logger.info("async history Data started: " + new Date() + " threadId:" + Thread.currentThread().getId());
        System.out.println("\nCargarHistorial(n) ");
        Thread.sleep(1000);
        itktoolService.sendHistoryData(fileServ,logger);
        logger.info("async history Data finished: " + new Date() + " threadId:" + Thread.currentThread().getId());
        return ;
    }
}
