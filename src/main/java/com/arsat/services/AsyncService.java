package com.arsat.services;

public interface AsyncService {
    void processNew() throws InterruptedException;
    void processEdit() throws InterruptedException;
    void processHistory() throws InterruptedException;
}
