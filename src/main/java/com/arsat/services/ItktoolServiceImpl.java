package com.arsat.services;

import com.arsat.UserItktoolXml;
import com.arsat.components.ExecuteShellCommand;
import com.arsat.entities.Empleado;
import com.arsat.entities.Historico;
import com.arsat.entities.Piso;
import com.arsat.entities.UserItktool;
import com.arsat.repositories.*;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItktoolServiceImpl implements ItktoolService {

    @Autowired
    private UserItktoolRepository repository;

    @Autowired
    private EmpleadoRepository repositoryEmpl;

    @Autowired
    private DispositivoRepository repositoryDisp;

    @Autowired
    private HistoricoRepository repositoryHist;

    @Autowired
    private PisoRepository repositoryPiso;

    @Autowired
    private ExecuteShellCommand executeShell;

    String itktool = "itktool -v -cn";
//TODO DEBO PROBAR SI FUNCIONA PARA EMPLEADO Y CREAR EL METODO DE VISTA
    @Override
    public void sendNewData(Logger log) {
        UserItktoolXml crear = new UserItktoolXml();
        List<Empleado> list;
        UserItktool userItktool = repository.findByEstado(1).get(0);
        list = repositoryEmpl.findByEstado(1);
        repositoryDisp.findAll().forEach(x ->
                {
                    if (x.getState() == 1) {

                        if (list != null && list.size() > 0) {
                            List<UserItktool> listPiso = new ArrayList<>();
                            Piso lisp;
                            for (Empleado user : list) {
                                userItktool.setAccess_id(user.getBadgeAccess().longValue());
                                userItktool.setUser_name(user.getName()+" "+user.getLastName());
                                userItktool.setTemplate_data1(user.getHuella1());
                                userItktool.setTemplate_data2(user.getHuella2());
                                user.getListPisos().forEach( p ->{
                                    if(p.getPisoId().equals(x.getPiso().id) && p.getState().equals(1)){
                                        listPiso.add(userItktool);
                                    }
                                   }
                                );

                            }
                            log.info("\nCrear Obj Nuevo UserItktoolXml");
                            crear.crearXml(listPiso, log, "/itktool/xmlusersau" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml");

                            String option = " -pp2 -nd1 -au/itktool/xmlusersau" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                            log.info("ejecutando archivo para agregar registro");
                            System.out.println(itktool + x.getIp() + ":" + x.getPort() + option);
                            executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + option, log);

                        }
                    } else {
                        System.out.println("\nfindByEstado(n)  1 -- agregado, sin registro");
                        log.info("\nfindByEstado(n)  2 -- modificado, sin registros");
                    }
                }
        );
        if (list != null && list.size() > 0) {
            for (Empleado user : list) {
                user.setState(0);
                repositoryEmpl.save(user);
            }
        }
    }
//TODO IGUAL QUE NUEVO
    @Override
    public void sendEditData(Logger log) {
        UserItktool userItktool = repository.findByEstado(1).get(0);
        List<Empleado> listupd;
        UserItktoolXml crear = new UserItktoolXml();
        Map<String,List<UserItktool>> mapPiso    = new HashMap<String,List<UserItktool>>();
        Map<String,List<UserItktool>> mapPisoDel = new HashMap<String,List<UserItktool>>();
        listupd = repositoryEmpl.findByEstado(2);
        repositoryDisp.findAll().forEach(x ->
                {
                    if (x.getState() == 1) {

                        if (listupd != null && listupd.size() > 0) {
                            List<UserItktool> listPisoDel = new ArrayList<>();
                            List<UserItktool> listPisoUpd = new ArrayList<>();
                            Piso lisp;
                            for (Empleado user : listupd) {
                                userItktool.setAccess_id(user.getBadgeAccess().longValue());
                                userItktool.setUser_name(user.getName()+" "+user.getLastName());
                                userItktool.setEstado(user.getEnabled()?1:0);
                                userItktool.setTemplate_data1(user.getHuella1());
                                userItktool.setTemplate_data2(user.getHuella2());
                                user.getListPisos().forEach(
                                    p ->{
                                        if(p.getPisoId().equals(x.getPiso().id) && p.getState().equals(1)){
                                            userItktool.setStatus(1);
                                            listPisoDel.add(userItktool);
                                            listPisoUpd.add(userItktool);
                                        }else
                                        if(p.getPisoId().equals(x.getPiso().id) && p.getState().equals(0)){
                                            userItktool.setStatus(0);
                                            listPisoDel.add(userItktool);
                                        }
                                    }
                                );

                            }

                            mapPisoDel.put (x.getPiso().getPiso(),listPisoDel);
                            mapPiso.put (x.getPiso().getPiso(),listPisoUpd);
                            log.info("\nCrear Obj modificar UserItktoolXml");
                            System.out.println("\nCrear Obj modificar UserItktoolXml");

                            crear.crearXml(mapPisoDel.get(x.getPiso().getPiso()),log, "/itktool/xmlusersdeu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml");
                            String optiondel = " -pp2 -nd1 -du/itktool/xmlusersdeu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                            System.out.println(itktool + x.getIp() + ":" + x.getPort() + optiondel);
                            executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + optiondel, log);

                            crear.crearXml(mapPiso.get(x.getPiso().getPiso()),log, "/itktool/xmluserseu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml");
                            String option = " -pp2 -nd1 -au/itktool/xmluserseu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                            log.info("ejecutando archivo para editar usuario");
                            System.out.println(itktool + x.getIp() + ":" + x.getPort() + option);
                            executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + option, log);

                        }
                    } else {
                        System.out.println("\nfindByEstado(n)  2 -- modificado, sin registro");
                        log.info("\nfindByEstado(n)  2 -- modificado, sin registros");
                    }
                }
        );
        if (listupd != null && listupd.size() > 0) {
            for (Empleado user : listupd) {
                user.setState(0);
                repositoryEmpl.save(user);
            }
        }

    }
// TODO HACER LOGICA COMO NUEVO
    @Override
    public void sendDeleteData(Logger log) {
        UserItktoolXml crear = new UserItktoolXml();
        UserItktool userItktool = repository.findByEstado(1).get(0);
        List<Empleado> list;
        list = repositoryEmpl.findByEstado(3);
        repositoryDisp.findAll().forEach(x ->
                {
                    if (x.getState()  == 1) {

                        if (list != null && list.size() > 0) {
                            List<UserItktool> listPiso = new ArrayList<>();
                            for (Empleado user : list) {
                                userItktool.setAccess_id(user.getBadgeAccess().longValue());
                                userItktool.setUser_name(user.getName()+" "+user.getLastName());
                                userItktool.setTemplate_data1(user.getHuella1());
                                userItktool.setTemplate_data2(user.getHuella2());
                                user.getListPisos().forEach(
                                        p -> {
                                            if (p.getPisoId().equals(x.getPiso().id) && p.getState().equals(1)) {
                                                userItktool.setStatus(0);
                                                listPiso.add(userItktool);
                                            }
                                        }
                                );
                            }
                            log.info("\nCrear Obj Nuevo UserItktoolXml");
                            crear.crearXml(listPiso, log, "/itktool/xmlusersdu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml");
                            //List<Dispositivo> disp =repositoryDisp.findAll();
                            String itktool = "itktool -v -cn";
                            String option = " -pp2 -nd1 -du/itktool/xmlusersdu" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                            log.info("ejecutando archivo para borrar registro");
                            System.out.println(itktool + x.getIp() + ":" + x.getPort() + option);
                            executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + option, log);
                        }
                    } else {
                        System.out.println("\nfindByEstado(n)  1 -- agregado, sin registro");
                        log.info("\nfindByEstado(n)  2 -- modificado, sin registros");
                    }
                }
        );
        if (list != null && list.size() > 0) {
            for (Empleado user : list) {
                user.setState(-1);
                repositoryEmpl.save(user);
            }
        }
    }

    @Override
    public void sendHistoryData(FileService fileServ,Logger log) {
        repositoryDisp.findAll().forEach(x ->
                {
                    if (x.getState()  == 1) {
                        //List<Dispositivo> disp =repositoryDisp.findAll();
                        String itktool = "itktool -v -cn";
                        String option = " -pp2 -nd1 -lh/itktool/xmlhistory" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                        log.info("ejecutando archivo para traer historial");
                        executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + option, log);
                        log.info("cargando archivo /itktool/xmlhistory" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml");
                        List<Historico> listAll = new ArrayList<Historico>();
                        List<Historico> listAux = new ArrayList<Historico>();
                        fileServ.cargarHistory(new File("/itktool/xmlhistory" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml"), listAll, x.getEntradaSalida(),x.getPiso());
                        listAll.forEach(li ->{
                            Historico hi = repositoryHist.findByhistorico(li.getAccess_id(),li.getEntrada_salida(),li.getFecha());
                            if(hi != null){
                                hi = null;
                            }else{
                                listAux.add(li);
                            }
                        });
                        log.info("Guardando Historico");
                        System.out.println("Guardando Historico");
                        repositoryHist.saveAll(listAux);
                        String optiondel = " -pp2 -nd1 -fh/itktool/xmlhistory" + x.getPiso().getPiso() + x.getEntradaSalida() + ".xml";
                        log.info("ejecutando archivo para borrado historial");
                        System.out.println("ejecutando archivo para borrado historial "+itktool + x.getIp() + ":" + x.getPort() + optiondel);;
                        executeShell.executeCommand(itktool + x.getIp() + ":" + x.getPort() + optiondel, log);
                    }
                }
        );
    }
//TODO agregar state =1 todos los pisos menos piso 3 igual esto ya no lo usaria
    @Override
    public void saveFull(List<UserItktool> listUser) {
        for (UserItktool user : listUser) {
            System.out.println("Guardando user" + user.getAccess_id());
            repository.save(user);
            Piso piso = new Piso();

            repositoryPiso.save(piso);

        }

    }
//TODO AGREGAR state=1 piso 3 en pisopermiso igual esto ya no lo usaria
    @Override
    public void savePiso3(List<UserItktool> listUser) {
        for (UserItktool user : listUser) {
            System.out.println("Guardando user" + user.getAccess_id());
            if(repository.existByAccess(user.getAccess_id()) != null){
             /* Piso p = repositoryPiso.findByAccess(user.getAccess_id().intValue()).get(0);
              p.setP3(true)
              repositoryPiso.save(p);;*/
            }else{
                repository.save(user);
                Piso piso = new Piso();
                repositoryPiso.save(piso);
            }

        }
    }
}
