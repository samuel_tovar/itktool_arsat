package com.arsat;

import com.arsat.services.FileService;
import com.arsat.services.ItktoolService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
//@ImportResource("classpath:itktool.appctx.xml")
public class StartApplication extends SpringBootServletInitializer {

    private static final Logger log = LoggerFactory.getLogger(StartApplication.class);

    @Autowired
    private FileService fileServ;
    @Autowired
    private ItktoolService itktool;

    public static void main(String[] args) {
        SpringApplication.run(StartApplication.class, args);
    }
/* implements CommandLineRunner
    @Override
    public void run(String... params) {
        log.info("StartApplication...");

        if(params.length == 1){
            log.info("Comenzando el proceso de lectura de archivo...");
            String envString  = params[0];
            if(envString.equals("saveFull")){
                try {
                        List<UserItktool> listUser = new ArrayList<UserItktool>();
                        log.info("cargando archivo users.xml");
                        fileServ.cargarXml(new File("/itktool/usersFull.xml"), listUser);
                        itktool.saveFull(listUser);
                  } catch (Exception e) {
                    e.printStackTrace();
                }
            }else
            if(envString.equals("savePiso3")){
                try {
                    List<UserItktool> listUser = new ArrayList<UserItktool>();
                    log.info("cargando archivo users.xml");
                    fileServ.cargarXml(new File("/itktool/usersPiso.xml"), listUser);
                    itktool.savePiso3(listUser);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            log.info("Proceso finalizado...");
        }


    }*/

}