package com.arsat.shedule;


import com.arsat.services.AsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Date;

@Component
@EnableScheduling
@EnableCaching
public class ScheduledUpdatesOnTopic {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AsyncService asyncService;



    @Scheduled(fixedRate = 20000)
    public void processNew() throws InterruptedException, IOException {
        logger.info("itktoolService New : started: " + new Date());
        asyncService.processNew();
        logger.info("itktoolService New : finished: " + new Date());
    }

    @Scheduled(fixedRate = 30000)
    public void processedit() throws InterruptedException, IOException {
        logger.info("itktoolService Edit: started: " + new Date());
        asyncService.processEdit();
        logger.info("itktoolService Edit: finished: " + new Date());
    }

    @Scheduled(fixedRate = 100000)
    public void processHistory() throws InterruptedException, IOException {
        logger.info("itktoolService History: started: " + new Date());
        asyncService.processHistory();
        logger.info("itktoolService History: finished: " + new Date());
    }

}
